class CreateElementos < ActiveRecord::Migration
  def change
    create_table :elementos do |t|
      t.string :nombre
      t.string :descipcion
      t.string :imagen
      t.integer :precio
      t.text :caracteristicas
      t.integer :tipo_id

      t.timestamps
    end
  end
end
